﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using System.Threading;

namespace BoardGame
{
    public partial class newGame : Form
    {
        private bool gotScore = false;
        private int score = 0;
        private ArrayList arrList = new ArrayList();
        private ArrayList txtToArray = new ArrayList();
        private string outputFile, row, colm;
        private int eskiKonum = 0, yeniKonum = 0, sekil, renk;
        private Random rnd = new Random();//Random sayı belirlemek fonsiyon olusturuldu.
        private string data;
        private Button[,] board; //Matriste kullanılan button[], artık global olarak kullanılabiliyor.
        private Image image;      //Butondaki şekli kaybetmemek için oluşturulan global değişken
        public int Row { get; set; } //Row ve Colm, matris fonksiyonundaki...
        public int Colm { get; set; } //...row ve colm ları kullanabilmek oluşturuldu.

        public newGame()
        {
            InitializeComponent();

            outputFile = Application.StartupPath + @"../../../userinfo.txt";    //dosyayı okumak için
            FileStream fileStream = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader reader = new StreamReader(fileStream);

            string satir = reader.ReadLine();
            data = satir;
            if (data == "custom")
            {
                row = reader.ReadLine();//customda girilen değerleri almak için
                colm = reader.ReadLine();//customda girilen değerleri almak için
            }

            while (satir != null)
            {
                txtToArray.Add(satir);
                satir = reader.ReadLine();
            }

            if (data == "" || data == "normal") //Normal boyutu için uygun oyun tahtası olusturuldu.
            {
                Matris(9, 9);
            }
            if (data == "easy")                    //Easy boyutu için uygun oyun tahtası olusturuldu.
            {
                Matris(15, 15);
            }
            if (data == "hard")                    //Hard boyutu için uygun oyun tahtası olusturuldu.
            {
                Matris(6, 6);
            }
            if (data == "custom")                  //Custom boyutu için uygun oyun tahtası olusturuldu.
            {
                Matris(Convert.ToInt32(row), Convert.ToInt32(colm));
            }

            reader.Close();
            fileStream.Close();
        }
        public void getScore(string dt)
        {
            switch (dt)
            {
                case "easy":
                    score += 1;

                    break;
                case "normal":
                    score += 3;

                    break;
                case "hard":
                    score += 5;

                    break;
                case "custom":
                    score += 2;

                    break;
            }
        }
        public void getPointVertical(int j)
        {
            int pointCondition = 1, deletedImageTag = 0;
            int firstIndex = -1, lastIndex = -1;

            for (int i = 0; i < this.Row; i++)
            {
                if (i != this.Row - 1 && board[i, j].BackgroundImage != null)
                {
                    for (int k = i + 1; k <= this.Row - 1; k++)
                    {
                        if (board[k, j].BackgroundImage != null && i != k)
                        {
                            if (Convert.ToInt32(board[i, j].BackgroundImage.Tag) == Convert.ToInt32(board[k, j].BackgroundImage.Tag))
                            {
                                deletedImageTag = Convert.ToInt32(board[i, j].BackgroundImage.Tag);

                                pointCondition++;

                                if (pointCondition >= 5)
                                {
                                    firstIndex = i;
                                    lastIndex = k;
                                }
                            }
                            else if (pointCondition < 5)
                            {
                                pointCondition = 1;
                                break;
                            }
                            else
                            {
                                lastIndex = k;
                                break;
                            }
                        } else if (board[k, j].BackgroundImage == null && pointCondition < 5)
                        {
                            pointCondition = 1;
                            break;
                        }
                        else if (board[k, j].BackgroundImage == null && pointCondition >= 5)
                        {
                            lastIndex = k;
                            break;
                        }
                    }
                }

                if (pointCondition >= 5)
                    break;
                else
                    pointCondition = 1;
            }

            if (pointCondition >= 5)
            {
                gotScore = true;

                for (int i = firstIndex; i <= lastIndex; i++)
                {
                    if (board[i, j].BackgroundImage != null && Convert.ToInt32(board[i, j].BackgroundImage.Tag) == deletedImageTag)
                    {
                        board[i, j].BackgroundImage = null;
                    }
                }

                getScore(data);
            }
        }
        public void getPointHorizontal(int i)
        {
            int pointCondition = 1, deletedImageTag = 0;
            int firstIndex = -1, lastIndex = -1;

            for (int j = 0; j < this.Colm; j++)
            {
                if (j != this.Colm - 1 && board[i, j].BackgroundImage != null)
                {
                    for (int k = j + 1; k <= this.Colm - 1; k++)
                    {
                        if (board[i, k].BackgroundImage != null && j != k)
                        {
                            if (Convert.ToInt32(board[i, j].BackgroundImage.Tag) == Convert.ToInt32(board[i, k].BackgroundImage.Tag))
                            {
                                deletedImageTag = Convert.ToInt32(board[i, j].BackgroundImage.Tag);

                                pointCondition++;

                                if (pointCondition >= 5)
                                {
                                    firstIndex = j;
                                    lastIndex = k;
                                }
                            }
                            else if (pointCondition < 5)
                            {
                                pointCondition = 1;
                                break;
                            }
                            else
                            {
                                lastIndex = k;
                                break;
                            }
                        }
                        else if (board[i, k].BackgroundImage == null && pointCondition < 5)
                        {
                            pointCondition = 1;
                            break;
                        }
                        else if (board[i, k].BackgroundImage == null && pointCondition >= 5)
                        {
                            lastIndex = k;
                            break;
                        }
                    }
                }

                if (pointCondition >= 5)
                    break;
                else
                    pointCondition = 1;
            }

            if (pointCondition >= 5)
            {
                gotScore = true;

                for (int j = firstIndex; j <= lastIndex; j++)
                {
                    if (board[i, j].BackgroundImage != null && Convert.ToInt32(board[i, j].BackgroundImage.Tag) == deletedImageTag)
                    {
                        board[i, j].BackgroundImage = null;
                    }
                }

                getScore(data);
            }
        }
        public bool isBoardFull()
        {
            int isFullCounter = 0;

            for (int i = 0; i < this.Row; i++)
                for (int j = 0; j < this.Colm; j++)
                    if (board[i, j].BackgroundImage != null)
                        isFullCounter++;
            if (isFullCounter == (this.Row * this.Colm) - 2)
                return true;
            else
                return false;
        }
        public void buttonCLick(object sender, EventArgs e)
        {
            Button pushed = sender as Button;
            
            if (pushed.BackgroundImage != null) //Basılan buton şekilliyse
            {
                image = pushed.BackgroundImage;

                for (int i = 0; i < Row; i++)
                {
                    for (int j = 0; j < Colm; j++)
                    {
                        if (pushed.Tag == board[i, j].Tag)
                        {
                            eskiKonum = Convert.ToInt32(pushed.Tag);        
                        }
                    }
                }

                pushed.BackgroundImage = null;

                for (int i = 0; i < Row; i++)
                {
                    for (int j = 0; j < Colm; j++)
                    {
                        board[i, j].Enabled = false;
                        
                        if (board[i, j].BackgroundImage == null)
                            board[i, j].Enabled = true;
                    }
                }
            }
            else //Basılan buton boşsa
            {
                pushed.BackgroundImage = image;

                for (int i = 0; i < Row; i++)
                {
                    for (int j = 0; j < Colm; j++)
                    {
                        if (pushed.Tag == board[i, j].Tag)
                        {
                            yeniKonum = Convert.ToInt32(pushed.Tag);
                        }
                    }
                }

                arrList.Remove(eskiKonum);
                arrList.Add(yeniKonum);
                arrList.Sort();

                Thread.Sleep(500);
                fillBoard();

                // Puan alma kısmı 
                for (int j = 0; j < this.Colm; j++)
                    this.getPointVertical(j); //dikeyde puan alma
                for (int i = 0; i < this.Row; i++)
                    this.getPointHorizontal(i); //yatayda puan alma
                //Puan alma kısmı

                for (int i = 0; i < Row; i++)
                {
                    for (int j = 0; j < Colm; j++)
                    {
                        board[i, j].Enabled = true;
                        
                        if (board[i, j].BackgroundImage == null)
                            board[i, j].Enabled = false;
                    }
                }

                SoundPlayer moveVoice = new SoundPlayer();//hamleden sonra ses eklendi.
                string konum = Application.StartupPath + @"../../../sounds/hamle.wav";
                
                moveVoice.SoundLocation = konum;

                SoundPlayer pointVoice = new SoundPlayer();
                string location = Application.StartupPath + @"../../../sounds/puan.wav";

                pointVoice.SoundLocation = location;

                //Playing some voice
                if (gotScore)
                {
                    pointVoice.Play();

                    if (lbScore.Text != "")
                    {
                        lbScore.Text = "";
                        lbScore.Text += score.ToString();
                    }
                    else
                    {
                        lbScore.Text += score.ToString();
                    }

                    gotScore = false;
                }
                else
                {
                    moveVoice.Play();
                }

                if (isBoardFull())
                {
                    MessageBox.Show("GAME OVER... Your score is: " + score);
                    this.Close();
                }
            }
        }

        public void fillBoard()
        {
            int SeklinKonumu_1, SeklinKonumu_2, SeklinKonumu_3;
            int tmp = (Row * Colm) + 1;

            while (true) //random hamlenin ust uste binmesi engellendi.
            {
                SeklinKonumu_1 = rnd.Next(1, tmp);
                SeklinKonumu_2 = rnd.Next(1, tmp);
                SeklinKonumu_3 = rnd.Next(1, tmp);

                if ((!arrList.Contains(SeklinKonumu_1)) && (!arrList.Contains(SeklinKonumu_2)) && (!arrList.Contains(SeklinKonumu_3)))
                {
                    arrList.Add(SeklinKonumu_1);
                    arrList.Add(SeklinKonumu_2);
                    arrList.Add(SeklinKonumu_3);
                    break;
                }
            }

            int counter = 0;

            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Colm; j++)
                {
                    counter++;

                    //bu kısımda random aralıkları belirlendi.
                    if (txtToArray.Contains("Square") && txtToArray.Contains("Triangle") && !txtToArray.Contains("Round")) sekil = rnd.Next(1, 3); // 1=square 2=triangle 3=round
                    if (txtToArray.Contains("Triangle") && txtToArray.Contains("Round") && !txtToArray.Contains("Square")) sekil = rnd.Next(2, 4);
                    if (txtToArray.Contains("Square") && txtToArray.Contains("Round") && !txtToArray.Contains("Triangle"))
                    {
                        while (true) //triangle çıkması engellendi.
                        {
                            sekil = rnd.Next(1, 4);
                            if (sekil != 2) 
                            {
                                break;
                            }
                        }
                    }
                    if (txtToArray.Contains("Square") && txtToArray.Contains("Triangle") && txtToArray.Contains("Round")) sekil = rnd.Next(1, 4);


                    //secilebilecek renklere göre random renk atama kombinasyonu uyarlandi.
                    if (txtToArray.Contains("red") && !txtToArray.Contains("blue") && !txtToArray.Contains("pink")) renk = 1;//1=red 2=blue 3=pink
                    if (txtToArray.Contains("blue") && !txtToArray.Contains("red") && !txtToArray.Contains("pink")) renk = 2;
                    if (txtToArray.Contains("pink") && !txtToArray.Contains("red") && !txtToArray.Contains("blue")) renk = 3;
                    if (txtToArray.Contains("red") && txtToArray.Contains("blue") && !txtToArray.Contains("pink")) renk = rnd.Next(1, 3);
                    if (txtToArray.Contains("blue") && txtToArray.Contains("pink") && !txtToArray.Contains("red")) renk = rnd.Next(2, 4);
                    if (txtToArray.Contains("red") && txtToArray.Contains("pink") && !txtToArray.Contains("blue"))
                    {
                        while (true) //blue bastırmamak icin yapildi.
                        {
                            renk = rnd.Next(1, 4);
                            if (renk != 2)
                            {
                                break;
                            }
                        }
                    }
                    if (txtToArray.Contains("red") && txtToArray.Contains("blue") && txtToArray.Contains("pink")) renk = rnd.Next(1, 4);


                    if ((counter == SeklinKonumu_1 || counter == SeklinKonumu_2 || counter == SeklinKonumu_3) && (sekil == 1))//birden fazla renk seciminde arralistte aratma yaparken karisiklik çikmamasi icin olusturuldu.
                    {
                        if (txtToArray.Contains("Square") && txtToArray.Contains("red") && renk == 1)    // Square & red
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/kirmizikare.JPG");
                            board[i, j].BackgroundImage.Tag = 5;
                        }
                        if (txtToArray.Contains("Square") && txtToArray.Contains("blue") && renk == 2)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/mavikare.JPG");
                            board[i, j].BackgroundImage.Tag = 4;
                        }
                        if (txtToArray.Contains("Square") && txtToArray.Contains("pink") && renk == 3)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/pembekare.JPG");
                            board[i, j].BackgroundImage.Tag = 6;
                        }
                    }

                    if ((counter == SeklinKonumu_1 || counter == SeklinKonumu_2 || counter == SeklinKonumu_3) && sekil == 2)
                    {
                        if (txtToArray.Contains("Triangle") && txtToArray.Contains("red") && renk == 1)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/kirmiziucgen.JPG");
                            board[i, j].BackgroundImage.Tag = 8;
                        }
                        if (txtToArray.Contains("Triangle") && txtToArray.Contains("blue") && renk == 2)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/maviucgen.JPG");
                            board[i, j].BackgroundImage.Tag = 7;
                        }
                        if (txtToArray.Contains("Triangle") && txtToArray.Contains("pink") && renk == 3)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/pembeucgen.JPG");
                            board[i, j].BackgroundImage.Tag = 9;
                        }
                    }

                    if ((counter == SeklinKonumu_1 || counter == SeklinKonumu_2 || counter == SeklinKonumu_3) && sekil == 3)
                    {
                        if (txtToArray.Contains("Round") && txtToArray.Contains("red") && renk == 1)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/kirmizidaire.JPG");
                            board[i, j].BackgroundImage.Tag = 2;//boyutlar 70,70 olarak butonlara uygun şekilde ayarlandı.
                        }
                        if (txtToArray.Contains("Round") && txtToArray.Contains("blue") && renk == 2)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/mavidaire.JPG");
                            board[i, j].BackgroundImage.Tag = 1;
                        }
                        if (txtToArray.Contains("Round") && txtToArray.Contains("pink") && renk == 3)
                        {
                            board[i, j].BackgroundImage = Image.FromFile(Application.StartupPath + @"../../../images/pembedaire.JPG");
                            board[i, j].BackgroundImage.Tag = 3;
                        }
                    }
                }
            }
        }
        public void Matris(int row, int colm)
        {
            this.Row = row;
            this.Colm = colm;

            int counter = 0;
            board = new Button[row, colm];
            int top = 0;
            int left = 0;

            int tagCount = 1; //Butonları işaretlemek için
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < colm; j++)
                {
                    counter++;
                    board[i, j] = new Button();
                    board[i, j].Width = 35;    //buton genisligi  varsayılan olarak atandi.
                    board[i, j].Height = 35;   //buton yüksekligi varsayılan olarak atandi.
                    board[i, j].Left = left;
                    board[i, j].Top = top;
                    board[i, j].Tag = tagCount++;
                    this.Controls.Add(board[i, j]);
                    left += 35;
                    board[i, j].BackColor = Color.White;//butonlara varsayılan olarak beyaz renk atandı.
                }
                top += 35;
                left = 0;
            }

            fillBoard();

            for (int i = 0; i < this.Row; i++)
            {
                for (int j = 0; j < this.Colm; j++)
                {
                    board[i, j].Click += new EventHandler(this.buttonCLick);

                    if (board[i, j].BackgroundImage == null)
                        board[i, j].Enabled = false;
                }
            }
        }
        private void newGame_Load(object sender, EventArgs e)
        {
            arrList.Sort();
            lbScore.Text = "0";
        }
    }
}
