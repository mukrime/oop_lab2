﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BoardGame
{
    public partial class helpScreen : Form
    {
        public helpScreen()
        {
            
            InitializeComponent();

            lblGameDef.Text = "There are 4 different difficulty levels in our game.\n The board is created according to the difficulty level. There are also shape and color options.";
            lblUsage.Text = "After the board is created, 3 random shapes and colors are added to the game board and the user is expected to move. \n After the move, 3 random shapes are added to the game board again.\n When the same 5 shapes and colors are brought together, the cells are empty and points are added.\n The game continues until the board is full.";
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            about ab = new about();
            
            if (ab.ShowDialog() == System.Windows.Forms.DialogResult.No)
                this.Close();
        }
    }
}
