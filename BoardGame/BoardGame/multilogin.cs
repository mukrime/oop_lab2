﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;


namespace BoardGame
{
    public partial class multilogin : Form
    {
        public multilogin()
        {
            InitializeComponent();
        }

        private void multilogin_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//connect
        {
            multiplayer newGame = new multiplayer(false, textBox1.Text);
            Visible = false;
            if (!newGame.IsDisposed)
                newGame.ShowDialog();
            Visible = true;
        }

        private void button2_Click(object sender, EventArgs e) //host
        {

            multiplayer newGame = new multiplayer(true);
            Visible = false;
            if (!newGame.IsDisposed)
                newGame.ShowDialog();
            Visible = true;

        }

      
    }
}
