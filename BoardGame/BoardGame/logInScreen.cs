﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Data.SqlClient;

namespace BoardGame
{
    public partial class logInScreen : Form
    {
        string outputFile;

        static public string userName;

        public logInScreen()
        {
            InitializeComponent();

            outputFile = "../../../user.txt";//dosyayı okumak için
            FileStream fileStream = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Read);
            //Bir sonraki çalıştırmada, kaydedilen ayarların sistem tarafından hatırlanması için
            using (StreamReader reader = new StreamReader(fileStream))
            {
                string satir = reader.ReadLine();
                if (satir != "")
                {
                    txtUsername.Text = satir;
                }
                reader.Close();
            }

            fileStream.Close();
        }
        SqlConnection baglanti;       //Kullanıcı verileri access veri tabanına tasindi.
        SqlCommand komut;
        SqlDataReader dr;

        private void logInScreen_Load(object sender, EventArgs e)
        {
            txtPassword.UseSystemPasswordChar = true;
            baglanti = new SqlConnection("server=.; Initial Catalog=userinfo;Integrated Security=SSPI");

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            lblError.Text = "";
        }
        private void btn_log_in_Click(object sender, EventArgs e)
        {
            string ad = txtUsername.Text;
            string parola = txtPassword.Text;
            komut = new SqlCommand();
            baglanti.Open();
            komut.Connection = baglanti;
            komut.CommandText= "SELECT *FROM userinfo WHERE Username='"+ad+"' AND Passwords='" + parola + "'";
            dr = komut.ExecuteReader();
            if (dr.Read())
            {
                Form2 settingsScreen = new Form2();
                settingsScreen.Show();
                this.Hide();
            }
            else
            {
                lblError.Text = "Invalid username or password!";
            }
            baglanti.Close();

            //XmlDocument xDoc = new XmlDocument();

            //xDoc.Load("../../../userinfo.xml");

            //XmlNode users = xDoc.SelectSingleNode("users");
            //XmlNodeList userInfos = users.SelectNodes("userInformation");

            //foreach (XmlNode info in userInfos)
            //{
            //    if (txtUsername.Text == info.SelectSingleNode("Username").InnerText &&
            //        register.sha256_hash(txtPassword.Text) == info.SelectSingleNode("Password").InnerText)
            //    {
            //        Form2 settingsScreen = new Form2();
            //        settingsScreen.Show();
            //        this.Hide();

            //        using (StreamWriter sW = new StreamWriter(outputFile))
            //        {
            //            sW.WriteLine(txtUsername.Text);
            //        }
            //    } 
            //    else
            //    {
            //        lblError.Text = "Invalid username or password!";
            //    }
            //}

            if ((txtUsername.Text == "user" || txtUsername.Text == "USER") &&
                (txtPassword.Text == "user" || txtPassword.Text == "USER"))
            {
                userName = txtUsername.Text;
                Form2 settingsScreen = new Form2();
                settingsScreen.Show();
                this.Hide();
            }
            else if ((txtUsername.Text == "admin" || txtUsername.Text == "ADMIN") &&
                (txtPassword.Text == "admin" || txtPassword.Text == "ADMIN")) {
                
                userName = txtUsername.Text;

                Form2 settingsScreen = new Form2();
                settingsScreen.Show();
                this.Hide();
            }
            else {
                lblError.Text = "Invalid username or password!";
            }

            using (StreamWriter writer = new StreamWriter(outputFile))
            {
                writer.WriteLine(txtUsername.Text);
            }
        }

        private void signup_Click(object sender, EventArgs e)
        {
            register rg = new register();
            rg.Show();
            this.Hide();
        }

        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                 && !char.IsSeparator(e.KeyChar);
        }
        
        private void chkBoxPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxPassword.Checked)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }
    }
}
