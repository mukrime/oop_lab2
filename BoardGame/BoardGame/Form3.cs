﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace BoardGame
{
    public partial class Form3 : Form
    {
        public static string userInfoFile;
        public static FileStream fileStream;

        public Form3()
        {
            InitializeComponent();

            userInfoFile = Application.StartupPath+ @"../../../userinfo.txt";//dosyayı okumak için
            fileStream = new FileStream(userInfoFile, FileMode.OpenOrCreate, FileAccess.Read); 
            //Bir sonraki çalıştırmada, kaydedilen ayarların sistem tarafından hatırlanması için
            using (StreamReader reader = new StreamReader(fileStream))
            {
                while (true) 
                {
                    string satir = reader.ReadLine();
                    if (satir == "")
                    {
                        easy.Checked = true;
                    }
                    if (satir == "easy")
                    {
                        easy.Checked = true;
                    }
                    if (satir == "normal")
                    {
                        normal.Checked = true;
                    }
                    if (satir == "hard")
                    {
                        hard.Checked = true;
                    }
                    if (satir == "custom")
                    {
                        custom.Checked = true;
                        satir = reader.ReadLine();
                        customRow.Text = satir;
                        satir = reader.ReadLine();
                        customCol.Text = satir;
                    }

                    //if (satir == "")
                    //{
                    //    shapeListBox1.SetItemChecked(0, true); //hiçbir şeçim yapılmazsa varsayılan olarak
                    //}
                    if (satir == "Square") //Square
                    {
                        shapeListBox1.SetItemChecked(0,true);
                    }
                    if (satir == "Triangle") //Triangle
                    {
                        shapeListBox1.SetItemChecked(1, true);
                    }
                    if (satir == "Round") //Round
                    {
                        shapeListBox1.SetItemChecked(2, true);
                    }

                    //renk

                    //if (satir == "")
                    //{
                    //    colorbox.SetItemChecked(0, true); //hiçbir şeçim yapılmazsa varsayılan olarak
                    //}
                    if (satir == "red") //red
                    {
                        colorbox.SetItemChecked(0, true);               
                    }
                    if (satir == "blue") //blue
                    {
                        colorbox.SetItemChecked(1, true);
                    }
                    if (satir == "pink") //pink
                    {
                        colorbox.SetItemChecked(2, true);
                    }
                    if (satir == null) break;
                }
                reader.Close();
            }
            fileStream.Close();
        }


        private void save_Click(object sender, EventArgs e)//kullanıcının seçimlerini kaydetmek için
        {
            bool isCorrect = true;

            if (!File.Exists(userInfoFile))
            {
                File.Create(userInfoFile);
            }

            //Burdan custom checked i kaldırmıştım.

            if (shapeListBox1.CheckedIndices.Count == 0 || shapeListBox1.CheckedIndices.Count == 1) //2 ve daha fazla sekil seçilmesi sartı saglandı.
            {
                isCorrect = false;
                lblMessage2.Text = "Please choose at least two shapes";
            }
            else
            {
                isCorrect = true;
                lblMessage2.Text = "";
            }

            if (colorbox.CheckedIndices.Count == 0)
            {
                isCorrect = false;
                lblMessage2.Text += Environment.NewLine + "You have to choose a color(s)";
            }

            //Special game case: Cannot choose 1 color and 1 shape. 06/05/2022
            if (colorbox.CheckedIndices.Count == 1 && shapeListBox1.CheckedIndices.Count == 1)
            {
                isCorrect = false;
                lblMessage2.Text = "You cannot choose 1 color and 1 shape only";
            }

            if (!easy.Checked && !normal.Checked && !hard.Checked && !custom.Checked)
            {
                lblMessage1.Text = "Please choose one of the difficulty levels.\n";
            }

            //Row check
            if (customRow.Text != "" && customRow.Visible == true && (Convert.ToInt32(customRow.Text) <= 5 || Convert.ToInt32(customRow.Text) > 15))
            {
                customRow.Text = "";

                lblCustomCheck.Text = "Inputs must be between 6 and 15!";
                isCorrect = false;
            }

            //Column check
            if (customCol.Text != "" && customCol.Visible == true && (Convert.ToInt32(customCol.Text) <= 5 || Convert.ToInt32(customCol.Text) > 15))
            {
                customCol.Text = "";

                lblCustomCheck.Text = "Inputs must be between 6 and 15!";
                isCorrect = false;
            }

            if (isCorrect)
            {
                using (StreamWriter writer = new StreamWriter(userInfoFile))
                {
                    if (easy.Checked)
                    {
                        writer.WriteLine(easy.Text);
                    }
                    if (normal.Checked)
                    {
                        writer.WriteLine(normal.Text);
                    }
                    if (hard.Checked)
                    {
                        writer.WriteLine(hard.Text);
                    }
                    if (custom.Checked)
                    {
                        writer.WriteLine(custom.Text);
                        if(customCol.Text != "" && customRow.Text != "")
                        {
                            writer.WriteLine(customRow.Text);
                            writer.WriteLine(customCol.Text);
                            lblMessage1.Text = "";
                        }
                        else if(customCol.Text == "" || customRow.Text == "")
                        {
                            lblMessage1.Text = "Error";
                        }
                    }

                    if (lblMessage1.Text != "Error")
                    {
                        writer.WriteLine("shape");
                        for (int i = 0; i < shapeListBox1.CheckedIndices.Count; i++) //şekil
                        {
                            if (shapeListBox1.CheckedIndices[i] == 0)
                            {
                                writer.WriteLine("Square");
                            }
                            if (shapeListBox1.CheckedIndices[i] == 1)
                            {
                                writer.WriteLine("Triangle");
                            }
                            if (shapeListBox1.CheckedIndices[i] == 2)
                            {
                                writer.WriteLine("Round");
                            }                            
                        }

                        writer.WriteLine("color");
                        for (int i = 0; i < colorbox.CheckedIndices.Count; i++) //renk seçilmezse hata vermiyor
                        {
                            if (colorbox.CheckedIndices[i] == 0)
                            {
                                writer.WriteLine("red");
                            }
                            if (colorbox.CheckedIndices[i] == 1)
                            {
                                writer.WriteLine("blue");
                            }
                            if (colorbox.CheckedIndices[i] == 2)
                            {
                                writer.WriteLine("pink");
                            }
                        }

                        lblMessage1.Text = "Saved";
                    }
                    //Form2 f2 = new Form2();
                    //f2.Show();
                    //this.Hide();
                }
            }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void shapeListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            lblMessage2.Text = "";
        }

        private void easy_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            lblMessage2.Text = "";
        }

        private void normal_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            lblMessage2.Text = "";
        }

        private void hard_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            lblMessage2.Text = "";
        }

        private void custom_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            if (custom.Checked)     //custom seçilirse textboxlar görünür oluyor.
            {
                customCol.Visible = true;
                customRow.Visible = true;
            }
            else
            {
                customCol.Visible = false;
                customRow.Visible = false;
            }
        }

        private void customRow_TextChanged(object sender, EventArgs e)
        {

        }

        private void customCol_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void customRow_KeyPress(object sender, KeyPressEventArgs e) //sadece sayı yazdırmak için
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void customCol_KeyPress(object sender, KeyPressEventArgs e) //sadece sayı yazdırmak için
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                e.Handled = true;
        }

        private void colorbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessage1.Text = "";
            lblMessage2.Text = "";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            this.Hide();
            f2.Show();
            this.Hide();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        //private void customRow_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int row = int.Parse(customRow.Text); 
        //    } catch
        //    {
        //        lblCustomError.Text = "Type numbers!";
        //    }
        //}

        //private void customCol_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int column = int.Parse(customCol.Text);
        //    }
        //    catch
        //    {
        //        lblCustomError.Text = "Type numbers!";
        //    }
        //}
    }
}
